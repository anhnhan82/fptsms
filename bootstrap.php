<?php
require_once realpath(__DIR__) . '/Autoload.php';

TechAPIAutoloader::register();

use TechAPI\Constant;
use TechAPI\Client;
use TechAPI\Auth\ClientCredentials;

// config api
Constant::configs(array(
    'mode'            => Constant::MODE_LIVE,
    //'mode'            => Constant::MODE_SANDBOX,
    'connect_timeout' => 15,
    'enable_cache'    => false,
    'enable_log'      => true,
    'log_path'    => realpath(__DIR__) . '/logs'
));


// config client and authorization grant type
function getTechAuthorization()
{
    /*
    $client = new Client(
        //'YOUR_CLIENT_ID',
        //'YOUR_CLIENT_SECRET',
        //'e615D85fc918f252e1754Ce2391c8Ef923AAB401',
        //'663642d023602e28784F8789dC939f14a54ece5f588848beBdd6314fab8c274de8B618a4',
        'c9403A2816A6c66Bb01e7c015fdF3a945d236715',
        '70fabdc650d712ef32cCa7A969dDF322777df7a791fa8f3063Ccb466a33860eC96553114',
        //array('send_mt_active'),
        array('send_brandname', 'send_brandname_otp')
    );
    */
    $client = new Client(
        '8694243aC28Ad2aa666456e0d6fc33B9b3A3c748',
        '50E42fE06875971022743449816dB232170d77de391a0a55A4fa32a01335026522937094',
        //array('send_mt_active')
        //array('send_brandname', 'send_brandname_otp', 'send_mt_active')
        //array('send_brandname')
        array('send_brandname_otp')
        //array('send_brandname', 'send_brandname_otp', 'send_mt_active')
    );
    
    return new ClientCredentials($client);
}